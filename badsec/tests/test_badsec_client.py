import pytest
from ..client import TOTAL_RETRIES_ALLOWED

TEST_USERS_LIST = ['12345678', '983974837654']


class AnyKindOfError(Exception):
    pass


class TestBadSec:
    def test_no_retry_required(self, badsec_client):
        self.attempts = 0

        @badsec_client.retry(exceptions=AnyKindOfError,
                             total_attempts=TOTAL_RETRIES_ALLOWED,
                             initial_wait=0.1)
        def success_from_the_first_attempt():
            self.attempts += 1
            return TEST_USERS_LIST

        response = success_from_the_first_attempt()
        assert response == TEST_USERS_LIST
        assert self.attempts == 1

    def test_retries_once(self, badsec_client):
        self.attempts = 0

        @badsec_client.retry(exceptions=AnyKindOfError,
                             total_attempts=TOTAL_RETRIES_ALLOWED,
                             initial_wait=0.1)
        def fails_once():
            self.attempts += 1
            if self.attempts < 2:
                raise AnyKindOfError('Dropped Connection')
            else:
                return TEST_USERS_LIST

        response = fails_once()

        assert response == TEST_USERS_LIST
        assert self.attempts == 2

    def test_limit_of_attempts_is_reached(self, badsec_client):
        self.attempts = 0

        @badsec_client.retry(exceptions=AnyKindOfError,
                             total_attempts=TOTAL_RETRIES_ALLOWED,
                             initial_wait=0.1)
        def must_fail():
            self.attempts += 1
            raise AnyKindOfError('Something bad happened')

        with pytest.raises(AnyKindOfError) as exc_info:
            must_fail()

        assert self.attempts == TOTAL_RETRIES_ALLOWED
