import sys
from functools import wraps
import time
from copy import deepcopy

import requests
import logging
from hashlib import sha256
from urllib.parse import urljoin
from requests.models import Response

logging.basicConfig(level=logging.INFO)

BADSEC_HEADER_NAME = 'Badsec-Authentication-Token'
REQUEST_FAILED_MESSAGE = 'The request has failed after'
RETRY_MESSAGE = 'Retrying in'
EXCEPTION_MESSAGE = 'Exception occured'
TOTAL_RETRIES_ALLOWED = 3


class BADSECAuth:
    BASE_URL = 'http://0.0.0.0:8888'
    BASE_AUTH_URL = 'auth'
    USERS_URL = 'users'

    def __init__(self):
        self.session = self._get_session()

    def abs_url(self, path) -> str:
        """
        Combine the base url with a path
        """
        return urljoin(self.BASE_URL, path)

    def _get_token_response(self) -> Response:
        """
        Get token response
        :return: instance of requests.models.Response class
        """
        res = requests.head(self.abs_url(self.BASE_AUTH_URL))
        return res

    def _calculate_checksum(self, token: str):
        """
        Method to calculate checksum
        :param token: auth token obtained by calling /auth endpoint
        :return: instance of _hashlib.HASH class
        """
        try:
            s = f'{token}/{self.USERS_URL}'.encode('utf-8')
            return sha256(s)
        except TypeError as err:
            logging.error(f'err')
            return None

    def _get_badsec_headers(self) -> dict:
        """
        Method to obtain headers that include
        required checksum
        :return: dict of custom headers
        """
        token = None
        res = self._get_token_response()
        if res.status_code == 200:
            token = res.headers.get(BADSEC_HEADER_NAME)
        checksum = self._calculate_checksum(token)
        if checksum:
            return {'X-Request-Checksum': checksum.hexdigest()}
        return {}

    def _get_session(self):
        """
        Method to create session object
        :return: requests.Session object
        """
        session = requests.Session()
        badsec_headers = self._get_badsec_headers()

        session.headers.update(badsec_headers)
        session.headers.update({'Accept': 'application/json'})
        session.headers.update({'Content-Type': 'application/json'})
        session.hooks = {
            'response': lambda r, *args, **kwargs: r.raise_for_status()
        }

        return session

    def get_users(self):
        return self._get(self.USERS_URL)

    def _get(self, url):
        """
        Method to provide interface for `get` requests.session method
        :param url:
        :return:
        """
        return self.session.get(urljoin(self.BASE_URL, url))


class BadsecClient:
    def __init__(self):
        self.auth_client = BADSECAuth()

    def retry(self, exceptions: [Exception], total_attempts: int = TOTAL_RETRIES_ALLOWED,
              initial_wait: float = 0.5, backoff_factor: int = 2):
        """
        Heavily inspired by https://wiki.python.org/moin/PythonDecoratorLibrary#Retry
        Method to call the decorated function with given or default settings
        :param exceptions: Any Exception(s) raised by an HTTPError object which returned by
            `response.raise_for_status()` if an error has occurred. And it will trigger a retry
        :param total_attempts: Total attempts allowed to be retried
        :param initial_wait: Time to first retry
        :param backoff_factor: Backoff multiplier that will be used in calculating delay for each retry).
        """

        def retry_decorator(f):
            @wraps(f)
            def func_with_retries(*args, **kwargs):
                _attempts, _delay = total_attempts + 1, initial_wait
                while _attempts > 1:
                    try:
                        return f(*args, **kwargs)
                    except exceptions as err:
                        _attempts -= 1
                        if _attempts == 1:
                            message = f'{REQUEST_FAILED_MESSAGE} ' \
                                      f'{total_attempts} attempts.\n'
                            logging.error(message)          
                            raise
                        message = f'{EXCEPTION_MESSAGE} {err}\n'
                        f'{RETRY_MESSAGE} {_delay} seconds...'
                        logging.error(message)
                        time.sleep(_delay)
                        _delay *= backoff_factor

            return func_with_retries

        return retry_decorator

    def get_users(self):
        """
        Method that wraps auth client `get_users`
        :return:
        """
        return self.auth_client.get_users()

    def _format_response(self, response_text):
        """
        Method to format response to be JSON-formatted list
        :param response:
        :return:
        """
        try:
            return response_text.splitlines()
        except AttributeError as exc:
            logging.error(str(exc))
            return Nones


if __name__ == "__main__":
    client = BadsecClient()

    retry_func = client.retry(Exception,
                              total_attempts=TOTAL_RETRIES_ALLOWED)
    wrapped_test_func = retry_func(client.get_users)
    try:
        r = wrapped_test_func()
        if r.status_code == 200:
            unformatted_values = deepcopy(r.text)
            formatted_response = \
                client._format_response(unformatted_values)
            logging.info(f'{formatted_response}')    
            exit(0)
    except Exception as error:
        logging.error(f'{error}')
        exit(1)
