import socket
import pytest
from requests import Session
from unittest.mock import patch

from .badsec.client import BadsecClient, BADSECAuth


def prevent_from_internet(*args, **kwargs):
    raise Exception("Access to Internet is restricted!")


socket.socket = prevent_from_internet

TEST_USERS_LIST = ['12345678', '983974837654']


@pytest.fixture
@patch('requests.get')
@patch('requests.head')
def badsec_auth(monkeypatch):
    auth_client = BADSECAuth()

    def mockreturn(*args, **kwargs):
        return Session()

    monkeypatch.setattr(auth_client, "session", mockreturn)
    yield auth_client


@pytest.fixture
@patch('requests.get')
@patch('requests.head')
def badsec_client(monkeypatch, badsec_auth):
    def mockreturn(*args, **kwargs):
        return badsec_auth

    client = BadsecClient()
    monkeypatch.setattr(client, "auth_client", mockreturn)
    return client
