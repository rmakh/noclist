install_pipx:
	python3 -m pip install pipx && python3 -m pipx ensurepath --force; \
	export PATH=$(python3 -m site --user-base)/bin:$PATH;
   
install_pipenv: install_pipx
	pipx install pipenv --force;

install_deps: install_pipenv
	pipenv install --deploy --ignore-pipfile;

run: install_deps	
	pipenv run python3 badsec/client.py;

tests: install_deps
	pipenv run pytest . -s